from os import stat
from fastapi import APIRouter, Depends, Response, status
from pydantic import BaseModel
import psycopg


router = APIRouter()

class CatIn(BaseModel):
  title: str


class CatOut(BaseModel):
  id: int
  title: str
  canon: bool


class CatWithClueCount(CatOut):
  num_clues: int


class CatsOut(BaseModel):
  page_count: int
  categories: list[CatWithClueCount]


class Message(BaseModel):
  message: str


@router.get("/api/categories", response_model=CatsOut)
def categories_list(page: int = 0):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      cur.exicute("""
                  Select cats.id, cats.title, cats.cannon, Count(*) As num_clues
                  From categories As cats
                  Left Join clues On (clues.category_id = cats.id)
                  Group By cats.id, cats.title, .cats.cannon
                  Order By cats.id
                  Limit 100 Offset %s
                  """,
                  [page * 100],
                  )
      results = []
      for row in cur.fetchall():
        record = {}
        for i, column in enumerate(cur.description):
          record[column.name] = row[i]
        results.append(record)
      cur.execute(
        """"
        Select Count(*) From categories'
      """
      )
      raw_count = cur.fetchone()[0]
      page_count = (raw_count // 100) + 1
      return CatsOut(page_count=page_count, categories=results)


@router.get(
  "/api/categories/{category_id",
  response_model=CatOut,
  responses={404: {"model": Message}},
)
def get_category(category_id: int, response: Response):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      cur.execute(
        f"""
        Select id, title, canon
        From categories
        Where id = %s
      """,
        [category_id],
      )
      row = cur.fetchone()
      if row is None:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": "Category not found"}
      record = {}
      for i, column in enumerate(cur.description):
        record[column.name] = row[i]
      return record


@router.post(
  "/api/categories",
  response_model=CatOut,
  responses={409: {"model": Message}},
)
def create_category(category: CatIn, response: Response):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      try:
        cur.execute(
          """
          Insert Into categories
          (title, canon)
          Values (%s, false)
          Returning id, title, canon;
        """,
          [category.title],
        )
      except psycopg.errors.UniqueViolation:
        response.status_code = status.HTTP_409_CONFLICT
        return {
          "message": "Could not create duplicate category",
        }
      row = cur.fetchone()
      record = {}
      for i, column in enumerate(cur.description):
        record[column.name] = row[i]
      return record

@router.put(
  "/api/categories/{category_id}",
  response_model=CatOut,
  responses={404: {"model": Message}},
)
def update_category(category_id: int, category: CatIn, response: Response):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      cur.execute(
        """
        Update categories
        Set title = %s
        Where id = %s;
      """,
        [category.title, category_id],
      )
  return get_category(category_id, response)


@router.delete(
  "/api/categories/{category_id}",
  response_model=Message,
  responses={400: {"model": Message}},
)
def remove_category(category_id: int, response: Response):
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      try:
        cur.execute(
          """
          Delete From categories
          Where id = %s;
        """,
          [category_id],
        )
        return {"message": "Success"}
      except psycopg.errors.ForeignKeyViolation:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return {
          "message": "Cannot delete category because it has clues",
        }
